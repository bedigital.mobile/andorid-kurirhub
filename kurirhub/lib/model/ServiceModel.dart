// ignore: import_of_legacy_library_into_null_safe
import 'package:firebase_database/firebase_database.dart';

class ServiceModel{

  String _key;
  String _promoAssetPath;
  String _promoType;
  String _promoName;
  double _discpercenttopoin;
  double _discamounttopoin;
  double _discpercenttoreward;
  double _discamounttoreward;
  String _expireddate;
  int _activedays;
  bool _active;
  String _promoLogoPath;
  String userId;

  ServiceModel(
      this._key,
      this._promoAssetPath,
      this._promoType,
      this._promoName,
      this._discpercenttopoin,
      this._discamounttopoin,
      this._discpercenttoreward,
      this._discamounttoreward,
      this._expireddate,
      this._activedays,
      this._active,
      this._promoLogoPath,
      this.userId,
      );

  //ServiceModel.empty();

  set promoAssetPath(String promoAssetPath) => this._promoAssetPath = promoAssetPath;
  String get promoAssetPath => this._promoAssetPath;

  set promoType(String promoType) => this._promoType = promoType;
  String get promoType => this._promoType;

  set promoName(String promoName) => this._promoName = promoName;
  String get promoName => this._promoName;

  set discpercenttopoin(double discpercenttopoin) => this._discpercenttopoin = discpercenttopoin;
  double get discpercenttopoin => this._discpercenttopoin;

  set discamounttopoin(double discamounttopoin) => this._discamounttopoin = discamounttopoin;
  double get discamounttopoin => this._discamounttopoin;

  set discpercenttoreward(double discpercenttoreward) => this._discpercenttoreward = discpercenttoreward;
  double get discpercenttoreward => this._discpercenttoreward;

  set discamounttoreward(double discamounttoreward) => this._discamounttoreward = discamounttoreward;
  double get discamounttoreward => this._discamounttoreward;

  set expireddate(String expireddate) => this._expireddate = expireddate;
  String get expireddate => this._expireddate;

  set activedays(int activedays) => this._activedays = activedays;
  int get activedays => this._activedays;

  set active(bool active) => this._active = active;
  bool get active => this._active;

  set promoLogoPath(String promoLogoPath) => this._promoLogoPath = promoLogoPath;
  String get promoLogoPath => this._promoLogoPath;

  Map<String, dynamic> toMap(){
    var map = Map<String, dynamic>();
    map = {
      'promoAssetPath': _promoAssetPath,
      'promoType': _promoType,
      'promoName': _promoName,
      'discpercenttopoin': _discpercenttopoin,
      'discamounttopoin': _discamounttopoin,
      'discpercenttoreward': _discpercenttoreward,
      'discamounttoreward': _discamounttoreward,
      'expireddate': _expireddate,
      //'expireddate': formatDate.format(_expireddate),
      'activedays': _activedays,
      'active': _active,
      'promoLogoPath': _promoLogoPath
    };

    //if(_id != null) map['id'] = _id;

    return map;
  }

  /*
  ServiceModel.map(Map object){
    this._promoAssetPath = object['promoAssetPath'];
    this._promoType = object['promoType'];
    this._promoName = object['promoName'];
    this._discpercenttopoin = object['discpercenttopoin'];
    this._discamounttopoin = object['discamounttopoin'];
    this._discpercenttoreward = object['discpercenttoreward'];
    this._discamounttoreward = object['discamounttoreward'];
    this._expireddate = object['expireddate'];
    this._activedays = object['activedays'];
    this._active = object['active'];
    this._promoLogoPath = object['promoLogoPath'];
  }
*/

  ServiceModel.fromSnapshot(DataSnapshot snapshot) :
        _key = snapshot.key,
        _promoAssetPath = snapshot.value["promoAssetPath"],
        _promoType = snapshot.value["promoType"],
        _promoName = snapshot.value["promoName"],
        _discpercenttopoin = double.parse(snapshot.value["discpercenttopoin"]==null?"0.0":snapshot.value["discpercenttopoin"].toString()),
        _discamounttopoin = double.parse(snapshot.value["discamounttopoin"]==null?"0.0":snapshot.value["discamounttopoin"].toString()),
        _discpercenttoreward = double.parse(snapshot.value["discpercenttoreward"]==null?"0.0":snapshot.value["discpercenttoreward"].toString()),
        _discamounttoreward = double.parse(snapshot.value["discamounttoreward"]==null?"0.0":snapshot.value["discamounttoreward"].toString()),
        _expireddate = snapshot.value["expireddate"],
        _activedays = snapshot.value["activedays"],
        _active = snapshot.value["active"],
        _promoLogoPath = snapshot.value["promoLogoPath"],
        userId = snapshot.value["userId"];


  toJson() {
    return {
      "promoAssetPath": promoAssetPath,
      "promoType": promoType,
      "promoName": promoName,
      "discpercenttopoin": discpercenttopoin,
      "discamounttopoin": discamounttopoin,
      "discpercenttoreward": discpercenttoreward,
      "discamounttoreward": discamounttoreward,
      "expireddate": expireddate,
      "activedays": activedays,
      "active": active,
      "promoLogoPath": promoLogoPath,
      "userId": userId,
    };
  }

}