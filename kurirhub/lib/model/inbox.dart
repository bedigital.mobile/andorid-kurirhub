
import 'package:firebase_database/firebase_database.dart';

class InboxModel{

  //final formatDate = DateFormat('yyyy-MM-dd');
  String _key;
  String _inboxSubject;
  String _inboxAssetPath;
  String _inboxType;
  double _discpercenttopoin;
  double _discamounttopoin;
  double _discpercenttoreward;
  double _discamounttoreward;
  String _expireddate;
  //DateTime _expireddate;
  int _activedays;
  bool _active;
  String _voucherLogoPath;
  String userId;
  String _message;
  String _linkurl;
  String _kodebooking;
  String _custid;
  String _serviceid;
  String _cabangid;
  String _barbermanid;
  //DateTime _tglbooking;
  String _tglbooking;
  String _modelid;
  String _itemid;
  String _jenisid;
  String _warnaid;
  String _ukuranid;
  String _statusbooking;
  String _keterangan;
  double _hargacash;
  double _hargapoin;
  double _hargareward;
  double _cashbackpoin;
  double _cashbackreward;
  bool _read;
  String _tgltransaksi;
  //DateTime _tgltransaksi;
  String _idbooking;

  InboxModel(
      this._key,
      this._inboxAssetPath,
      this._inboxType,
      this._inboxSubject,
      this._discpercenttopoin,
      this._discamounttopoin,
      this._discpercenttoreward,
      this._discamounttoreward,
      this._expireddate,
      this._activedays,
      this._active,
      this._voucherLogoPath,
      this.userId,
      this._message,
      this._linkurl,
      this._kodebooking,
      this._custid,
      this._serviceid,
      this._cabangid,
      this._barbermanid,
      this._tglbooking,
      this._modelid,
      this._itemid,
      this._jenisid,
      this._warnaid,
      this._ukuranid,
      this._statusbooking,
      this._keterangan,
      this._hargacash,
      this._hargapoin,
      this._hargareward,
      this._cashbackpoin,
      this._cashbackreward,
      this._read,
      this._tgltransaksi,
      this._idbooking
      );

  //InboxModel.empty();

  set key(String key) => this._key = key;
  String get key => this._key;

  set inboxAssetPath(String inboxAssetPath) => this._inboxAssetPath = inboxAssetPath;
  String get inboxAssetPath => this._inboxAssetPath;

  set inboxType(String inboxType) => this._inboxType = inboxType;
  String get inboxType => this._inboxType;

  set inboxSubject(String inboxSubject) => this._inboxSubject = inboxSubject;
  String get inboxSubject => this._inboxSubject;

  set discpercenttopoin(double discpercenttopoin) => this._discpercenttopoin = discpercenttopoin;
  double get discpercenttopoin => this._discpercenttopoin;

  set discamounttopoin(double discamounttopoin) => this._discamounttopoin = discamounttopoin;
  double get discamounttopoin => this._discamounttopoin;

  set discpercenttoreward(double discpercenttoreward) => this._discpercenttoreward = discpercenttoreward;
  double get discpercenttoreward => this._discpercenttoreward;

  set discamounttoreward(double discamounttoreward) => this._discamounttoreward = discamounttoreward;
  double get discamounttoreward => this._discamounttoreward;


  //set expireddate(DateTime expireddate) => this._expireddate = expireddate;
  //DateTime get expireddate => this._expireddate;


  set expireddate(String expireddate) => this._expireddate = expireddate;
  String get expireddate => this._expireddate;

  set activedays(int activedays) => this._activedays = activedays;
  int get activedays => this._activedays;

  set active(bool active) => this._active = active;
  bool get active => this._active;

  set voucherLogoPath(String voucherLogoPath) => this._voucherLogoPath = voucherLogoPath;
  String get voucherLogoPath => this._voucherLogoPath;

  set message(String message) => this._message = message;
  String get message => this._message;

  set linkurl(String linkurl) => this._linkurl = linkurl;
  String get linkurl => this._linkurl;


  set kodebooking(String kodebooking) => this._kodebooking = kodebooking;
  String get kodebooking => this._kodebooking;

  set custid(String custid) => this._custid = custid;
  String get custid => this._custid;

  set serviceid(String serviceid) => this._serviceid = serviceid;
  String get serviceid => this._serviceid;

  set cabangid(String cabangid) => this._cabangid = cabangid;
  String get cabangid => this._cabangid;

  set barbermanid(String barbermanid) => this._barbermanid = barbermanid;
  String get barbermanid => this._barbermanid;

  //set tglbooking(DateTime tglbooking) => this._tglbooking = tglbooking;
  //DateTime get tglbooking => this._tglbooking;

  set tglbooking(String tglbooking) => this._tglbooking = tglbooking;
  String get tglbooking => this._tglbooking;

  set modelid(String modelid) => this._modelid = modelid;
  String get modelid => this._modelid;

  set itemid(String itemid) => this._itemid = itemid;
  String get itemid => this._itemid;

  set jenisid(String jenisid) => this._jenisid = jenisid;
  String get jenisid => this._jenisid;

  set warnaid(String warnaid) => this._warnaid = warnaid;
  String get warnaid => this._warnaid;

  set ukuranid(String ukuranid) => this._ukuranid = ukuranid;
  String get ukuranid => this._ukuranid;

  set statusbooking(String statusbooking) => this._statusbooking = statusbooking;
  String get statusbooking => this._statusbooking;

  set keterangan(String keterangan) => this._keterangan = keterangan;
  String get keterangan => this._keterangan;

  set hargacash(double hargacash) => this._hargacash = hargacash;
  double get hargacash => this._hargacash;

  set hargapoin(double hargapoin) => this._hargapoin = hargapoin;
  double get hargapoin => this._hargapoin;

  set hargareward(double hargareward) => this._hargareward = hargareward;
  double get hargareward => this._hargareward;

  set cashbackpoin(double cashbackpoin) => this._cashbackpoin = cashbackpoin;
  double get cashbackpoin => this._cashbackpoin;

  set cashbackreward(double cashbackreward) => this._cashbackreward = cashbackreward;
  double get cashbackreward => this._cashbackreward;

  set read(bool read) => this._read = read;
  bool get read => this._read;

  //set tgltransaksi(DateTime expireddate) => this._tgltransaksi = tgltransaksi;
  //DateTime get tgltransaksi => this._tgltransaksi;

  set tgltransaksi(String expireddate) => this._tgltransaksi = tgltransaksi;
  String get tgltransaksi => this._tgltransaksi;

  set idbooking(String expireddate) => this._idbooking = idbooking;
  String get idbooking => this._idbooking;

  Map<String, dynamic> toMap(){
    var map = Map<String, dynamic>();
    map = {
      'inboxAssetPath': _inboxAssetPath,
      'inboxType': _inboxType,
      'inboxSubject': _inboxSubject,
      'discpercenttopoin': _discpercenttopoin,
      'discamounttopoin': _discamounttopoin,
      'discpercenttoreward': _discpercenttoreward,
      'discamounttoreward': _discamounttoreward,
      'expireddate': _expireddate,
      //'expireddate': formatDate.format(_expireddate),
      'activedays': _activedays,
      'active': _active,
      'voucherLogoPath': _voucherLogoPath,
      'message': _message,
      'linkurl': _linkurl,
      'kodebooking': _kodebooking,
      'custid': _custid,
      'serviceid': _serviceid,
      'cabangid': _cabangid,
      'barbermanid': _barbermanid,
      'tglbooking': _tglbooking,
      'modelid': _modelid,
      'itemid': _itemid,
      'jenisid': _jenisid,
      'active': _active,
      'warnaid': _warnaid,
      'ukuranid': _ukuranid,
      'statusbooking': _statusbooking,
      'keterangan': _keterangan,
      'hargacash': _hargacash,
      'hargapoin': _hargapoin,
      'hargareward': _hargareward,
      'cashbackpoin': _cashbackpoin,
      'cashbackreward': _cashbackreward,
      'read': _read,
      'tgltransaksi': tgltransaksi,
      'idbooking': idbooking
    };
    return map;
  }


  //InboxModel.fromSnapshot(DataSnapshot snapshot) {
  InboxModel.fromSnapshot(DataSnapshot snapshot) :
    _key = snapshot.key,
    _inboxAssetPath = snapshot.value["inboxAssetPath"],
    _inboxType = snapshot.value["inboxType"],
    _inboxSubject = snapshot.value["inboxSubject"],
    _discpercenttopoin = double.parse(snapshot.value["discpercenttopoin"].toString()),
    _discamounttopoin = double.parse(snapshot.value["discamounttopoin"].toString()),
    _discpercenttoreward = double.parse(snapshot.value["discpercenttoreward"].toString()),
    _discamounttoreward = double.parse(snapshot.value["discamounttoreward"].toString()),
    _expireddate = snapshot.value["expireddate"],
    _activedays = snapshot.value["activedays"],
    _active = snapshot.value["active"],
    _voucherLogoPath = snapshot.value["voucherLogoPath"],
    userId = snapshot.value["userId"],
    _message = snapshot.value["message"],
    _linkurl = snapshot.value["linkurl"],
    _kodebooking = snapshot.value["kodebooking"],
    _custid = snapshot.value["custid"],
    _serviceid = snapshot.value["serviceid"],
    _cabangid = snapshot.value["cabangid"],
    _barbermanid = snapshot.value["barbermanid"],
    _tglbooking = snapshot.value["tglbooking"],
    _modelid = snapshot.value["modelid"],
    _itemid = snapshot.value["itemid"],
    _jenisid = snapshot.value["jenisid"],
    _warnaid = snapshot.value["warnaid"],
    _ukuranid = snapshot.value["ukuranid"],
    _statusbooking = snapshot.value["statusbooking"],
    _keterangan = snapshot.value["keterangan"],
    _hargacash = double.parse(snapshot.value["hargacash"].toString()),
    _hargapoin = double.parse(snapshot.value["hargapoin"].toString()),
    _hargareward = double.parse(snapshot.value["hargareward"].toString()),
    _cashbackpoin = double.parse(snapshot.value["cashbackpoin"].toString()),
    _cashbackreward = double.parse(snapshot.value["cashbackreward"].toString()),
    _read = snapshot.value["read"],
    _tgltransaksi = snapshot.value["tgltransaksi"]==null?"":snapshot.value["tgltransaksi"],
    _idbooking = snapshot.value["idbooking"]==null?"":snapshot.value["idbooking"];



  toJson() {return {
    "inboxAssetPath": inboxAssetPath,
    "inboxType": inboxType,
    "inboxSubject": inboxSubject,
    "discpercenttopoin": discpercenttopoin,
    "discamounttopoin": discamounttopoin,
    "discpercenttoreward": discpercenttoreward,
    "discamounttoreward": discamounttoreward,
    "expireddate": expireddate,
    "activedays": activedays,
    "active": active,
    "voucherLogoPath": voucherLogoPath,
    "userId": userId,
    "message": message,
    "linkurl": linkurl,
    "kodebooking": kodebooking,
    "serviceid": serviceid,
    "cabangid": cabangid,
    "barbermanid": barbermanid,
    "tglbooking": tglbooking,
    "modelid": modelid,
    "itemid": itemid,
    "jenisid": jenisid,
    //"active": active,
    "warnaid": warnaid,
    "ukuranid": ukuranid,
    "statusbooking": statusbooking,
    "keterangan": keterangan,
    "hargacash": hargacash,
    "hargapoin": hargapoin,
    "hargareward": hargareward,
    "cashbackpoin": cashbackpoin,
    "cashbackreward": cashbackreward,
    "read": read,
    "tgltransaksi": tgltransaksi,
    "idbooking": idbooking,
  };
  }

}