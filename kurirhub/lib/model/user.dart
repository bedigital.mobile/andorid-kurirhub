
import 'package:firebase_database/firebase_database.dart';
class Users {
  String _id;
  String _telepon;
  String _nama;
  String _namaawal;
  String _namaakhir;
  String _namauser;
  String _password;
  String _pin;
  String _email;
  String _nik;
  String _alamat;
  String _jenis;
  String _nowhatsapp;
  String _bank;
  String _account;
  DateTime _tgldaftar;
  bool _aktif;
  int _notification;


  Users(this._id, this._telepon, this._nama, this._namaawal, this._namaakhir, this._namauser,this._password,
      this._pin,this._email, this._nik, this._alamat, this._jenis, this._nowhatsapp, this._bank,this._account,
      this._tgldaftar, this._aktif, this._notification);

  /*
  Users.map(dynamic obj) {
    this._id = obj['id'];
    this._telepon = obj['telepon'];
    this._nama = obj['nama'];
    this._namaawal = obj['namaawal'];
    this._namaakhir = obj['namaakhir'];
    this._namauser = obj['nama'];
    this._password = obj['email'];
    this._pin = obj['nik'];
    this._email = obj['email'];
    this._nik = obj['nik'];
    this._alamat = obj['alamat'];
    this._jenis = obj['jenis'];
    this._nowhatsapp = obj['nowhatsapp'];
    this._bank = obj['bank'];
    this._account = obj['account'];
    this._saldocash = obj['saldocash'];
    this._saldoreward = obj['saldoreward'];
    this._saldopoin = obj['saldopoin'];
    this._tgldaftar = obj['tgldaftar'];
    this._aktif = obj['aktif'];
    this._notification = obj['notification'];
  }
*/

  String get id => _id;
  String get telepon => _telepon;
  String get nama => _nama;
  String get namaawal => _namaawal;
  String get namaakhir => _namaakhir;
  String get namauser => _namauser;
  String get password => _password;
  String get pin => _pin;
  String get email => _email;
  String get nik => _nik;
  String get alamat => _alamat;
  String get jenis => _jenis;
  String get nowhatsapp => _nowhatsapp;
  String get bank => _bank;
  String get account => _account;
  DateTime get tgldaftar => _tgldaftar;
  bool get aktif => _aktif;
  int get notification => _notification;

  Users.fromSnapshot(DataSnapshot snapshot) :
    _id = snapshot.key,
    _telepon = snapshot.value["telepon"]==null?"":snapshot.value['telepon'],
    _nama = snapshot.value["nama"]==null?"":snapshot.value['nama'],
    _namaawal = snapshot.value["namaawal"]==null?"":snapshot.value['namaawal'],
    _namaakhir = snapshot.value["namaakhir"]==null?"":snapshot.value['namaakhir'],
    _namauser = snapshot.value["namauser"]==null?"":snapshot.value['namauser'],
    _password = snapshot.value["password"]==null?"":snapshot.value['password'],
    _pin = snapshot.value["pin"]==null?"":snapshot.value['pin'],
    _email = snapshot.value["email"]==null?"":snapshot.value['email'],
    _nik = snapshot.value["nik"]==null?"":snapshot.value['nik'],
    _alamat = snapshot.value["alamat"]==null?"":snapshot.value['alamat'],
    _jenis = snapshot.value["jenis"]==null?"":snapshot.value['jenis'],
    _nowhatsapp = snapshot.value["nowhatsapp"]==null?"":snapshot.value['nowhatsapp'],
    _bank = snapshot.value["bank"]==null?"":snapshot.value['bank'],
    _account = snapshot.value["account"]==null?"":snapshot.value['account'],

    _tgldaftar =   DateTime(int.parse(snapshot.value['tgldaftar'].toString().split("-")[0]),
      int.parse(snapshot.value['tgldaftar'].toString().split("-")[1]),
      int.parse(snapshot.value['tgldaftar'].toString().split("-")[2].substring(0,2)),
      int.parse(snapshot.value['tgldaftar'].toString().split(" ")[1].split(":")[0]),
      int.parse(snapshot.value['tgldaftar'].toString().split(" ")[1].split(":")[1]),
      int.parse(snapshot.value['tgldaftar'].toString().split(" ")[1].split(":")[2].split(".")[0]),
    ),
    _aktif = snapshot.value['aktif']==null?false:snapshot.value['aktif'],
    _notification = snapshot.value['notification']==null?0:snapshot.value['notification'];

}
