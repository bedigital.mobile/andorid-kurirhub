import 'dart:async';
//import 'package:booky/main.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';

abstract class BaseAuth {
  Future<String> signIn(String email, String password);

  Future<User> signInUser(String email, String password);

  Future<String> signUp(String email, String password);

  Future<User> signUpUser(String email, String password);

  Future<void> signUpPhone(String email, String password, String name, String phone);

  Future<User> getCurrentUser();

  Future<void> sendEmailVerification();

  Future<void> signOut();

  Future<bool> isEmailVerified();
}

class Auth implements BaseAuth {
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  late UserCredential _authResult;

  late User user;

  Future<String> signIn(String email, String password) async {
    //FirebaseUser user = await _firebaseAuth.signInWithEmailAndPassword(
    //    email: email, password: password);
    _authResult = await _firebaseAuth.signInWithEmailAndPassword(
        email: email, password: password);
    User user = _authResult.user!;
    return user.uid;
  }

  Future<User> signInUser(String email, String password) async {
    //FirebaseUser user = await _firebaseAuth.signInWithEmailAndPassword(
    //    email: email, password: password);
    _authResult = await _firebaseAuth.signInWithEmailAndPassword(
        email: email, password: password);
    User user = _authResult.user!;
    return user;
  }
  Future<String> signUp(String email, String password) async {

    //FirebaseUser user = await _firebaseAuth.createUserWithEmailAndPassword(
    //    email: email, password: password);
    _authResult = await _firebaseAuth.createUserWithEmailAndPassword(
        email: email, password: password);
    User user = _authResult.user!;
    return user.uid;
  }

  Future<User> signUpUser(String email, String password) async {
    try {
      _authResult = await _firebaseAuth.createUserWithEmailAndPassword(
        email: email, password: password).catchError((error) {
        Fluttertoast.showToast(
            //msg: error.toString(),
            msg: error.toString().contains("ERROR_EMAIL_ALREADY_IN_USE")?"Email Sudah terdaftar, harap gunakan email lain atau lakukan Sign-In":
            error.toString(),
            //toastLength: Toast.LENGTH_SHORT,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 4,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
      );
        //print(error);
        /*
        _firebaseAuth.signInWithEmailAndPassword(email: email, password: password).then((user)
        {
          return user;
        }
        );
        */
        return null;
    });
      User user = _authResult.user!;
    return user;
    } on PlatformException catch (e) {
      Fluttertoast.showToast(
          msg: e.message!,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 4,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0
      );
      //print(e.toString());
      //print(e.code.toString());
      //print(e.details.toString());
      //return null;
      return user;
    }
  }

  Future<void> signUpPhone(String email, String password, String name, String phone) async {
    //UserUpdateInfo info = new UserUpdateInfo();
    //info.displayName = name.toString();
    _firebaseAuth.createUserWithEmailAndPassword(email: email, password: password)
        .then(
      //(user)=>{
            (authresult)=>{
          //if(user != null){
          if(authresult.user != null){
            //user.updateProfile(info),
            //authresult.user.updateProfile(info),
            authresult.user!.updateProfile(displayName:name.toString())
          }
        });
  }

  Future<User> getCurrentUser() async {
    /*
    FirebaseUser user = await _firebaseAuth.currentUser();
    return user;
*/
/*
    FirebaseUser user = await _firebaseAuth.currentUser().then((user) {
      //setState(() {
        if (user != null) {
          user.reload();
        }
      });
    //});
 */
    //user = await _firebaseAuth.currentUser();
    User user = (await _firebaseAuth.currentUser)!;
    return user;
    /*
    FirebaseUser user = await _firebaseAuth.currentUser();
    await user.reload();
    user = await _firebaseAuth.currentUser();
    //bool flag = user.isEmailVerified;
    return user;
    */
  }

  Future<void> signOut() async {
    return _firebaseAuth.signOut();
  }

  Future<void> sendEmailVerification() async {
    //FirebaseUser user = await _firebaseAuth.currentUser();
    User user = (await _firebaseAuth.currentUser)!;
    user.sendEmailVerification();
  }

  Future<bool> isEmailVerified() async {
    //FirebaseUser user = await _firebaseAuth.currentUser();
    //return user.isEmailVerified;

    //FirebaseUser user = await _firebaseAuth.currentUser();
    User user = _firebaseAuth.currentUser!;
    await user.reload();
    //user = await _firebaseAuth.currentUser();
    user = (await _firebaseAuth.currentUser)!;
    //bool flag = user.isEmailVerified;
    //return user.isEmailVerified;
    return user.emailVerified;
  }

}
