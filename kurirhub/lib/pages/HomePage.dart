import 'package:flutter/material.dart';
import 'package:flutter_app/animation/ScaleRoute.dart';
import 'package:flutter_app/pages/SignInPage.dart';
import 'package:flutter_app/widgets/BestFoodWidget.dart';
import 'package:flutter_app/widgets/BottomNavBarWidget.dart';
import 'package:flutter_app/widgets/PopularFoodsWidget.dart';
import 'package:flutter_app/widgets/SearchWidget.dart';
import 'package:flutter_app/widgets/TopMenus.dart';

import 'package:flutter_app/widgets/SearchWidgetSimple.dart';
import 'package:flutter_app/widgets/Promo.dart';
import 'package:flutter_app/widgets/Highlights.dart';
import 'package:flutter_app/pages/PackageDeliveryTrackingPage.dart';
import 'package:flutter_app/pages/SenderInfo.dart';
import 'package:flutter_app/services/authentication.dart';
import 'package:flutter_app/pages/HomePage.dart';
import 'package:flutter_app/pages/InboxPage.dart';
import 'package:flutter_app/pages/TransHistPage.dart';
import 'package:flutter_app/pages/AccountPage.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class HomePage extends StatefulWidget {
  HomePage({required this.auth, required this.userId, required this.name,required this.email,required this.password,
    required this.phone,required this.isEmailVerified,required this.isAnonymous, required  this.indexpage
  });

  final BaseAuth auth;
  final String userId;
  final String name;
  final String email;
  final String password;
  final String phone;
  final String isEmailVerified;
  final String isAnonymous;
  final int indexpage;
  @override
  _HomePageState createState() => _HomePageState();
}
class _HomePageState extends State<HomePage> {
  late List<Widget> pages;
  late Widget currentPage;
  final PageStorageBucket bucket = PageStorageBucket();
  int currentTab = 0;
  //late HomePage homepage;
  late Widget homewidget;
  late InboxPage inboxpage;
  late TransHistPage transhistpage;
  late AccountPage accountpage;

  @override
  void initState() {
    super.initState();

    /*
    homepage=new HomePage(
        indexpage: 0, auth: widget.auth, userId: widget.userId, name: widget.name, email: widget.email, password: widget.password,
        phone: widget.phone, isEmailVerified: widget.isEmailVerified, isAnonymous: widget.isAnonymous
    );
*/
    homewidget=new SingleChildScrollView(
      child: Column(
        children: <Widget>[
          //SearchWidget(),
          TopMenus(),
          //PopularFoodsWidget(),
          Highlight(),
          //BestFoodWidget(),
          Promo(),
        ],
      ),
    );

    inboxpage=new InboxPage(auth: widget.auth, userId: widget.userId, name: widget.name, email: widget.email,
        password: widget.password, phone: widget.phone, isEmailVerified: widget.isEmailVerified, isAnonymous: widget.isAnonymous);

    transhistpage=new TransHistPage(auth: widget.auth, userId: widget.userId, name: widget.name, email: widget.email, password: widget.password,
        phone: widget.phone, isEmailVerified: widget.isEmailVerified, isAnonymous: widget.isAnonymous);

    accountpage=new AccountPage(auth: widget.auth, userId: widget.userId, name: widget.name, email: widget.email, password: widget.password,
        phone: widget.phone, isEmailVerified: widget.isEmailVerified, isAnonymous: widget.isAnonymous);

    //pages=[ homepage, inboxpage, transhistpage, accountpage];
    pages=[ homewidget, inboxpage, transhistpage, accountpage];

    //currentPage =  pages[widget.indexpage];
    //currentPage =  pages[1];
    setState(() {
      currentTab=0;
      currentPage=pages[0];
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFFFAFAFA),
        elevation: 0,
        leading: Padding(
        //padding: EdgeInsets.only(left: 10,top: 5,right: 10,bottom: 5),
        padding: EdgeInsets.only(left: 10,top: 0,right: 0,bottom: 0),
        child:
            Image.asset(
                  "assets/images/fastku.jpg",
              fit: BoxFit.contain,
            )
        ),
        title: SearchWidgetSimple(),
        brightness: Brightness.light,

        actions: <Widget>[
          IconButton(
              icon: Icon(
                Icons.notifications_none,
                color: Color(0xFF3a3737),
              ),
              //onPressed: () {Navigator.push(context, ScaleRoute(page: SignInPage()));})
              onPressed: () {Navigator.push(context, ScaleRoute(page: SenderInfo()));})
              //onPressed: () {Navigator.push(context, ScaleRoute(page: PackageDeliveryTrackingPage()));})

        ],


      ),

/*
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            //SearchWidget(),
            TopMenus(),
            //PopularFoodsWidget(),
            Highlight(),
            //BestFoodWidget(),
            Promo(),
          ],
        ),
      ),
*/


      body:PageStorage(
          bucket: bucket,
          child: currentPage),


/*
      bottomNavigationBar: BottomNavBarWidget(indexpage: 0, auth: widget.auth, userId: widget.userId, name: widget.name, email: widget.email, password: widget.password,
          phone: widget.phone, isEmailVerified: widget.isEmailVerified, isAnonymous: widget.isAnonymous),
*/

      bottomNavigationBar:new BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          selectedFontSize: 12,
          currentIndex: currentTab,
          onTap: (int index){
            setState(() {
              currentTab=index;
              currentPage=pages[index];
            });
            //_updateUserKey(widget.userId, 0);
          },
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text(
              'Beranda',
              style: TextStyle(color: Color(0xFF2c2b2b)),
            ),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.mail),
            title: Text(
              'Kotak Masuk',
              style: TextStyle(color: Color(0xFF2c2b2b)),
            ),

          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.card_giftcard),
            title: Text(
              'Transaksi Saya',
              style: TextStyle(color: Color(0xFF2c2b2b)),
            ),
          ),
          BottomNavigationBarItem(
            icon: Icon(FontAwesomeIcons.user),
            title: Text(
              'Akun',
              style: TextStyle(color: Color(0xFF2c2b2b)),
            ),
          ),
        ],
      ),

    );
  }
}
