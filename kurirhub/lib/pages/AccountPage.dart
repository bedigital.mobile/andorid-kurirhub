import 'package:flutter_app/main.dart';
import 'package:flutter/material.dart';
//import 'package:booky/widgets/bank_card.dart';
import 'package:flutter_app/services/authentication.dart';
import 'package:firebase_database/firebase_database.dart';

//import '../util/dictionnary.dart' as dictionnary;
//import 'package:booky/widgets/settings.dart';
//import '../database/settings_model.dart';
import 'package:flutter_app/services/settings_model.dart';
import 'package:flutter_app/services/db_handler.dart';
import 'package:flutter_app/pages/TransHistPage.dart';
//import 'package:booky/viewcontroller/promo_page.dart';
//import 'package:booky/viewcontroller/inbox_page.dart';
//import 'package:booky/viewcontroller/voucher_page.dart';
//import 'package:booky/pages/root_page.dart';

class AccountPage extends StatefulWidget {
  AccountPage({required this.auth,required this. userId,required this.name,required this.email,required this.password,required this.phone, required this.isEmailVerified,
    required this.isAnonymous});
  final BaseAuth auth;
  final String userId;
  final String name;
  final String email;
  final String password;
  final String phone;
  final String isEmailVerified;
  final String isAnonymous;

  @override
  AccountPageState createState() => AccountPageState();
}

class AccountPageState extends State<AccountPage> {

  //double saldocash;
  //double saldoreward;
  //double saldopoin;
  late Map<dynamic, dynamic> values;

  //List<BankCardModel> cards;

  String _selectedLang = "";
  final dbh = DatabaseHandler.internal();

  void initState() {
    super.initState();

    var _db = FirebaseDatabase.instance.reference().child("users");
    _db.child(widget.userId).once().then((DataSnapshot snapshot) {
      values = snapshot.value;
      print(values.toString());
      values.forEach((k, v) {
        if (v["id"].toString() == widget.userId) {
          //saldocash = double.parse(v["saldocash"] == null? "0.0":v["saldocash"].toString());
          //saldoreward = double.parse(v["saldoreward"] == null? "0.0":v["saldoreward"].toString());
          //saldopoin = double.parse(v["saldopoin"] == null? "0.0":v["saldopoin"].toString());
        }
      }
      );
    }
    );

/*
    cards = [
      BankCardModel('images/bg_red_card.png', widget.name == null? "":widget.name,
          widget.phone == null? "":widget.phone, 'O-CASH', saldocash),
      BankCardModel('images/bg_blue_circle_card.png', widget.name == null? "":widget.name,
          widget.phone == null? "":widget.phone, 'O-POIN', saldopoin),
      /*
      BankCardModel('images/bg_purple_card.png', widget.name == null? "":widget.name,
          widget.phone == null? "":widget.phone, 'O-REWARD', saldoreward),
       */
    ];
*/
  }



  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      //backgroundColor: Color(0xFFF4F4F4),
      /*
      appBar: AppBar(
        title: Text('Profile'),
        //backgroundColor: color.primary,

        actions: <Widget>[
          //Container(),
          FlatButton(
            child: Icon(Icons.settings, color: Colors.white),
            onPressed: _showSettings,
          ),
          /*
          FlatButton(
            child: Icon(Icons.list, color: Colors.white),
            onPressed: (){
              _goToAllTasksScreen(context);
            },
          ),
*/
          FlatButton(
            child: Icon(Icons.history, color: Colors.white),
            onPressed: (){
              _goToHistoryScreen(context);
            },
          ),

          // PopupMenuButton<String>(
          //   itemBuilder: (BuildContext context){
          //     return <PopupMenuItem<String>>[
          //       PopupMenuItem<String>(
          //         child: Text('Parameters'),
          //         value: 'settings',
          //       ),
          //       PopupMenuItem<String>(
          //         child: Text('All stuff'),
          //         value: 'list_stuff'
          //       ),
          //     ];
          //   },
          //   onSelected: (value){
          //     if(value == 'list_stuff') _goToAllTasksScreen(context);
          //   },
          // )
        ],
        //flexibleSpace: _buildcard(),
      ),
      */
      appBar: AppBar(
        //title: const Text("Odysen Barbershop"),
        actions: <Widget>[
            new FlatButton(
                child: new Text('Logout',
                    style: new TextStyle(fontSize: 17.0, color: Colors.white)),
                //onPressed: _onWillPop
                onPressed: () {
                },
            ),
          IconButton(
            icon: Icon(Icons.settings),
            onPressed: () {
              //_showSettings();
            },
          ),
          /*
          IconButton(
            icon: Icon(Icons.history),
            onPressed: () {
              _goToHistoryScreen(context);
            },
          ),
          */
          IconButton(
            //icon: Icon(Icons.email),
            icon: Icon(Icons.local_offer),
            onPressed: () {
              _goToInboxScreen(context);
            },
          ),

          /*
          IconButton(
            icon: Icon(Icons.local_offer),
            onPressed: () {
              _goToPromoScreen(context);
            },
          ),
*/
        ],
      ),
      body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            /*
            Padding(
              padding:
                  const EdgeInsets.only(top: 30.0, left: 16.0, right: 16.0),
              child: Text(
                //'My Profile',
                'Profile',
                style: TextStyle(fontWeight: FontWeight.w700, fontSize: 20.0),
              ),
            ),
            */
            Container(
              child: Expanded(
                child: ListView.builder(
                    itemCount: 2,
                    itemBuilder: (BuildContext context, int index) {
                      if (index == 0) {
                        return Container(
                          margin: EdgeInsets.only(left: 16.0, right: 16.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(top: 8.0),
                                child: Padding(
                                  padding: const EdgeInsets.all(12.0),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.stretch,
                                    children: <Widget>[
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(bottom: 16.0),
                                        child: Row(
                                          children: <Widget>[
                                            Expanded(
                                              child: Text(
                                                widget.name.toString(),
                                                style: TextStyle(
                                                    fontWeight:
                                                        FontWeight.w700),
                                              ),
                                            ),
                                            /*
                                            GestureDetector(
                                              child: Row(
                                                children: <Widget>[
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            right: 4.0),
                                                    child: Text('Edit'),
                                                  ),
                                                  Icon(
                                                    Icons.edit,
                                                    size: 10.0,
                                                  )
                                                ],
                                              ),
                                            )
                                            */
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(bottom: 16.0),
                                        child: Row(
                                          children: <Widget>[
                                            Expanded(
                                              child: Text(
                                                //'Phone: 0937110938',
                                                "Phone: " + widget.phone.toString(),
                                                style: TextStyle(
                                                    fontWeight:
                                                        FontWeight.w700),
                                              ),
                                            ),
                                            /*
                                            GestureDetector(
                                              child: Row(
                                                children: <Widget>[
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            right: 4.0),
                                                    child: Text('Edit'),
                                                  ),
                                                  Icon(
                                                    Icons.edit,
                                                    size: 10.0,
                                                  )
                                                ],
                                              ),
                                            )
                                            */
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(bottom: 16.0),
                                        child: Row(
                                          children: <Widget>[
                                            Expanded(
                                              child: Text(
                                                //'Email: longhoang.2984@gmail.com',
                                                "Email: " + widget.email.toString(),
                                                style: TextStyle(
                                                    fontWeight:
                                                        FontWeight.w700),
                                              ),
                                            ),
                                            /*
                                            GestureDetector(
                                              child: Row(
                                                children: <Widget>[
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            right: 4.0),
                                                    child: Text('Edit'),
                                                  ),
                                                  Icon(
                                                    Icons.edit,
                                                    size: 10.0,
                                                  )
                                                ],
                                              ),
                                            )
                                            */
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              )
                            ],
                          ),
                        );
                      } else {
                        //return _userBankCardsWidget();
                        return Container();
                      }
                    }),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _goToHistoryScreen(BuildContext context) async {
    await Navigator.of(context).push(
        MaterialPageRoute(
            //builder: (_) => AllStuffWidget()
            builder: (_) =>
            /*
              HistoryPage(
                userId: widget.userId,
                auth: widget.auth,
              )
                */
          TransHistPage(auth: widget.auth, userId: widget.userId, name: widget.name, email: widget.email,
              password: widget.password, phone: widget.phone, isEmailVerified: widget.isEmailVerified, isAnonymous: widget.isAnonymous)
        )
    );
  }

  void _goToInboxScreen(BuildContext context) async {
    await Navigator.of(context).push(
        MaterialPageRoute(
            builder: (_) =>

                TransHistPage(auth: widget.auth, userId: widget.userId, name: widget.name, email: widget.email,
                    password: widget.password, phone: widget.phone, isEmailVerified: widget.isEmailVerified, isAnonymous: widget.isAnonymous)

        )
    );
  }

  /*
  void _goToPromoScreen(BuildContext context) async {
    await Navigator.of(context).push(
        MaterialPageRoute(
          //builder: (_) => AllStuffWidget()
            builder: (_) =>
                PromoPage(
                  userId: widget.userId,
                  auth: widget.auth,
                  //onSignedOut: _onSignedOut,
                )
        )
    );
  }

  Widget _userBankCardsWidget() {
    var size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.only(left: 16.0, right: 16.0),
//      height: 400.0,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
              padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 15.0),
              child: Row(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(right: 8.0),
                    child: Icon(Icons.account_balance),
                  ),
                  //Text('Saldo, Poin & Reward',
                  Text('Saldo & Poin',
                  style: TextStyle(fontWeight: FontWeight.w700),)
                ],
              )
          ),

          GridView.count(crossAxisCount: size.width > 320 ? 2 : 1,
          physics: ScrollPhysics(),
          shrinkWrap: true,
          childAspectRatio: (152 / 92),
            controller: new ScrollController(keepScrollOffset: false),
          children: List.generate(cards.length, (index) {
            return _getBankCard(index);
          }),),
        ],
      ),
    );
  }
*/

  void _saveSettings() async{
    Settings settings = await dbh.readSettings();
    settings.lang = _selectedLang;
    dbh.saveSettings(settings);
    //dictionnary.lang = _selectedLang;
    setState(() {});
  }

  /*
  void _showSettings() {
    showDialog(
      context: context,
      barrierDismissible: false, // user must tap button for close dialog!
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: Text(dictionnary.translate('settings')),
          content: SettingsWidget(
            selectedLang: dictionnary.lang,
            onDataChanged: (lang) => _selectedLang = lang,
          ),
          actions: <Widget>[
            FlatButton(
              child: Text(dictionnary.translate('save')),
              onPressed: () {
                _saveSettings();
                Navigator.of(context).pop();

              },
            ),
            FlatButton(
              child: Text(dictionnary.translate('cancel')),
              onPressed: () => Navigator.of(context).pop(), //Navigator.pop(context),
            )
          ],
        );
      },
    );
  }

  Future<bool> _onWillPop() {
    return showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text(dictionnary.translate('are.you.sure')),
        content: new Text(dictionnary.translate('logout')),
        actions: <Widget>[
          new FlatButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: new Text(dictionnary.translate('no')),
          ),
          new FlatButton(
            //onPressed: () => _signOut,
            onPressed: () {
              _signOut();
              //Navigator.of(context).pop();
              Navigator.of(context).popUntil((route) => route.isFirst);
              Navigator.pushReplacement(
                  context,
                  new MaterialPageRoute(

                      //builder: (context) => new RootPage(auth: new Auth(),)
                      builder: (context) => new MyApp()
                  ));
            },
            child: new Text(dictionnary.translate('yes')),
          ),
        ],
      ),
    ) ?? false;
  }
*/
  _signOut() async {
    try {
      await widget.auth.signOut();
      //widget.onSignedOut();
    } catch (e) {
      print(e);
    }
  }

}
