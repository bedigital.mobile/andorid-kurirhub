import 'package:flutter/material.dart';
import 'package:flutter_app/services/authentication.dart';
//import 'package:booky/viewcontroller/homeplace_page.dart';
import 'package:flutter_app/pages/HomePage.dart';
import 'package:flutter_app/model/transactionhistory.dart';
import 'package:firebase_database/firebase_database.dart';
import 'dart:async';
//import 'package:date_format/date_format.dart';
//import 'package:intl/intl.dart';


class TransHistPage extends StatefulWidget {

  TransHistPage({required this.auth, required this.userId, required this.name, required this.email, required this.password, required this.phone, required this.isEmailVerified,
    required this.isAnonymous});
  final BaseAuth auth;
  final String userId;
  final String name;
  final String email;
  final String password;
  final String phone;
  final String isEmailVerified;
  final String isAnonymous;

  @override
  State<StatefulWidget> createState() => new TransHistPageState();
}

enum AuthStatus {
  NOT_DETERMINED,
  NOT_LOGGED_IN,
  LOGGED_IN,
}


String _userId = "";

class TransHistPageState extends State<TransHistPage> {
  List<TransactionHistoryModel> _inboxList = [];
  List<TransactionHistoryModel> _inboxListreserved = [];
  final FirebaseDatabase _database = FirebaseDatabase.instance;
  late StreamSubscription<Event> _onTransactionHistoryAddedSubscription;
  late StreamSubscription<Event> _onTransactionHistoryChangedSubscription;
  late Query _inboxQuery;

  late String title;
  AuthStatus authStatus = AuthStatus.NOT_DETERMINED;
  final _textEditingController = TextEditingController();
  late DateTime date_time;

  bool isLoggedIn = false;
  String name = '';

  late Map<dynamic, dynamic> values;
  late String nama;
  late String email;
  late String password;
  String keyuser = "";
  double saldo = 0;
  late String photoUrl, phonenumber, providerId, isEmailVerified, isAnonymous;
  late String telepon;

  final userReference = FirebaseDatabase.instance.reference().child('users');

  void initState() {
    super.initState();
    //_inboxList = new List();
    List _inboxList = [];
    _inboxQuery = _database
        .reference()
        .child("inbox")
        .orderByChild("userId")
        .equalTo(widget.userId);
    _onTransactionHistoryAddedSubscription = _inboxQuery.onChildAdded.listen(_onEntryAdded);
    _onTransactionHistoryChangedSubscription = _inboxQuery.onChildChanged.listen(_onEntryChanged);

    widget.auth.getCurrentUser().then((user) {
      setState(() {
        _userId = user.uid;
        authStatus = user.uid == null ? AuthStatus.NOT_LOGGED_IN : AuthStatus.LOGGED_IN;
      });
    });
  }

  @override
  void dispose() {
    _onTransactionHistoryAddedSubscription.cancel();
    _onTransactionHistoryChangedSubscription.cancel();
    super.dispose();
  }

  _onEntryChanged(Event event) {
    var oldEntry = _inboxList.singleWhere((entry) {
      //return entry.key  == event.snapshot.key;
      return entry.key  == event.snapshot.key;
    });

    setState(() {
      _inboxList[_inboxList.indexOf(oldEntry)] = TransactionHistoryModel.fromSnapshot(event.snapshot);
    });
  }

  _onEntryAdded(Event event) {
    setState(() {
      _inboxList.add(TransactionHistoryModel.fromSnapshot(event.snapshot));
    });
  }

  _signOut() async {
    try {
      await widget.auth.signOut();
      //widget.onSignedOut();
    } catch (e) {
      print(e);
    }
  }


  _updateTransactionHistory(TransactionHistoryModel inbox){
    //inbox.read= !inbox.read;
    inbox.read= true;
    if (inbox != null) {
      //_database.reference().child("master").child("inbox").child(inbox.key).set(inbox.toJson());
      _database.reference().child("inbox").child(inbox.key).set(inbox.toJson());
      //_updateUserKey(widget.userId, 0);
    }
  }

  _updateUserKey(String key, int notif){
    if (key != "") {
      userReference.child(key).update({
        'notification': notif,
      });
    }
  }


  _deleteTransactionHistory(String inboxId, int index) {
    //_database.reference().child("master").child("inbox").child(inboxId).remove().then((_) {
    _database.reference().child("inbox").child(inboxId).remove().then((_) {
      //_database.reference().child("inbox").child(inboxId).remove().then((_) {
      print("Delete $inboxId successful");
      setState(() {
        _inboxList.removeAt(index);
      });
    });
  }


  void _onLoggedIn() {
    widget.auth.getCurrentUser().then((user){
      setState(() {
        //userId = user.uid.toString();
        _userId = user.uid.toString();
      });
    });
    setState(() {
      authStatus = AuthStatus.LOGGED_IN;

    });
  }

  void _onSignedOut() {
    setState(() {
      authStatus = AuthStatus.NOT_LOGGED_IN;
      _userId = "";
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      //backgroundColor: Color(0xFFF4F4F4),
      body:_showTransactionHistoryList(),
      /*
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          _showDialog(context);
        },
        tooltip: 'Increment',
        child: Icon(Icons.add),
      )
      */
    );
  }

  Widget _showTransactionHistoryList() {
    if (_inboxList.length > 0) {
      _inboxListreserved = _inboxList.reversed.toList();
      return ListView.builder(
          shrinkWrap: true,
          itemCount: _inboxList.length,
          itemBuilder: (BuildContext context, int index) {
            String inboxId = _inboxListreserved[index].key;
            String name = _inboxListreserved[index].inboxSubject;

            String expired =  _inboxListreserved[index].expireddate;
            String transdate =  _inboxListreserved[index].tgltransaksi;
            bool active = _inboxListreserved[index].active;
            String userId = _inboxListreserved[index].userId;
            String type = _inboxListreserved[index].inboxType;
            bool read = _inboxListreserved[index].read;
            //------------
            Padding(
                padding:
                EdgeInsets.only(top: 0.0, left: 8.0, right: 8.0, bottom: 4.0),
                child: OutlineButton.icon(
                  onPressed: () {
                    //Navigator.push(
                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                          /*
                              Home(
                            userId: userId,
                            name: name,
                            email: email,
                            password: password,
                            isEmailVerified: isEmailVerified,
                            isAnonymous: isAnonymous,
                            auth: widget.auth,
                            onSignedOut: _onSignedOut,
                            indexpage: 0
                          )
                              */
                          HomePage(indexpage: 0, auth: widget.auth, userId: widget.userId, name: widget.name, email: widget.email, password: widget.password,
                              phone: widget.phone, isEmailVerified: widget.isEmailVerified, isAnonymous: widget.isAnonymous)
                      ),
                    );
                  },

                  icon: Icon(Icons.forward),
                  label: Text('Booking Services'),
                  //textColor: Colors.black,
                  borderSide: BorderSide(color: Colors.blue),
                )
            );
            //------------

            return Dismissible(
              key: Key(inboxId),
              background: Container(color: Colors.red),
              onDismissed: (direction) async {
                _deleteTransactionHistory(inboxId, index);
              },
              child: (type.toLowerCase() == 'voucher')
                  ?
              ListTile(
                title: Text(
                  name,
                  style: TextStyle(fontSize: 20.0),
                ),
                subtitle: Text(
                  expired.length>=10?transdate.substring(0,10):"",
                  style: TextStyle(fontSize: 20.0),
                ),

                trailing:
                OutlineButton.icon(
                  onPressed: () {
                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                          /*
                                  Home(
                                userId: userId,
                                name: name,
                                email: email,
                                password: password,
                                isEmailVerified: isEmailVerified,
                                isAnonymous: isAnonymous,
                                auth: widget.auth,
                                onSignedOut: _onSignedOut,
                              )
                               */
                          HomePage(indexpage: 0, auth: widget.auth, userId: widget.userId, name: widget.name, email: widget.email, password: widget.password,
                              phone: widget.phone, isEmailVerified: widget.isEmailVerified, isAnonymous: widget.isAnonymous)
                      ),
                    );
                  },
                  icon: Icon(Icons.forward),
                  label: Text('Pakai'),
                  //textColor: Colors.black,
                  borderSide: BorderSide(color: Colors.blue),
                ),
              )
                  :
              ListTile(
                title: Text(
                  name,
                  style: TextStyle(fontSize: 20.0),
                ),

                subtitle: Text(
                  //expired,
                  transdate.length>=10?transdate.substring(0,10):"",
                  style: TextStyle(fontSize: 20.0),
                ),
              ),

            );
          });
    } else {
      return Container();
    }
  }
  void nothing ()
  {
  }

  Widget _voucherWidget(TransactionHistoryModel inbox) {
    return Container(
//      height: 100.0,
      margin: EdgeInsets.only(bottom: 16.0, left: 16.0, right: 16.0),
      child: Card(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 16.0),
                child: Image.asset(
                  inbox.inboxAssetPath,
                  height: 40.0,
                  width: 40.0,
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        inbox.inboxType,
                        style: TextStyle(fontWeight: FontWeight.bold),
                        textAlign: TextAlign.left,
                      ),
                      Text(inbox.inboxSubject)
                    ],
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        '\$${inbox.discpercenttopoin}',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Expanded(
                            child: Text(
                              'Debit dari \non ${inbox.expireddate}',
                              textAlign: TextAlign.right,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 8.0, top: 4.0),
                            child: Image.asset(
                              inbox.voucherLogoPath,
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
