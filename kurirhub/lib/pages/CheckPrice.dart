import 'package:flutter/material.dart';
import 'package:flutter_app/animation/ScaleRoute.dart';
import 'package:flutter_app/pages/SignInPage.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter_app/widgets/SearchAddressWidget.dart';

import 'package:flutter_app/widgets/Dimension.dart';

import 'package:flutter_app/model/ServiceModel.dart';


class Services extends StatefulWidget {
  @override
  _ServicesState createState() => _ServicesState();
}

class _ServicesState extends State<Services> {
  //Snapshot snapshot;
  @override
  Widget build(BuildContext context) {
    //List<DocumentSnapshot> loadedDoctors = [];

    List<ServiceModel> loadedDoctors = [];

    bool isLoading = false;

    return Scaffold(
      appBar: AppBar(
        title: Text('Info Pengiriman'),
        centerTitle: true,
      ),
      body: Container(
        margin: const EdgeInsets.only(
          bottom: 20.0,
        ),
        child: Column(
          children: <Widget>[
            loadedDoctors.length == null
                ? Center(
              child: Text('No More Data to load...'),
            )
                : ListView.builder(
              reverse: true,
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemCount: loadedDoctors.length,
              itemBuilder: (context, index) {
                /*
                return doctorCard(
                  context: context,
                  firstName: doctorSnapshot.docs[index]
                      .data()["firstName"],
                  lastName: doctorSnapshot.docs[index]
                      .data()["lastName"],
                  prefix: doctorSnapshot.docs[index]
                      .data()["prefix"],
                  specialty: doctorSnapshot.docs[index]
                      .data()["specialty"],
                  imagePath: doctorSnapshot.docs[index]
                      .data()["imagePath"],
                  rank: doctorSnapshot.docs[index]
                      .data()["rank"],
                );

                 */
                return new Container();
              },
            ),
            isLoading
                ? Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.all(5),
              color: Colors.yellowAccent,
              child: Text(
                'Loading',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
            )
                : Container()
          ],
        ),
      ),
    );
  }

}

class SaveButtonWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: new BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(5.0)),
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: Color(0xFFfbab66),
          ),
          BoxShadow(
            color: Color(0xFFf7418c),
          ),
        ],
        gradient: new LinearGradient(
            colors: [Color(0xFFf7418c), Color(0xFFfbab66)],
            begin: const FractionalOffset(0.2, 0.2),
            end: const FractionalOffset(1.0, 1.0),
            stops: [0.0, 1.0],
            tileMode: TileMode.clamp),
      ),
      child: MaterialButton(
          highlightColor: Colors.transparent,
          splashColor: Color(0xFFf7418c),
          //shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(5.0))),
          child: Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 10.0, horizontal: 42.0),
            child: Text(
              "SIMPAN",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 25.0,
                  fontFamily: "WorkSansBold"),
            ),
          ),
          onPressed: () => {}),
    );
  }
}



