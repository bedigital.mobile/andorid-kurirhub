import 'package:flutter/material.dart';
import 'package:flutter_app/services/authentication.dart';
//import 'package:booky/pages/home_page.dart';
//import 'package:booky/viewcontroller/landing_page.dart';
//import 'package:booky/viewcontroller/homeplace_page.dart';
import 'package:shared_preferences/shared_preferences.dart';
//import 'package:booky/viewcontroller/register_page.dart';
//import 'package:booky/view/login/login.dart';
import 'package:firebase_database/firebase_database.dart';

//import 'package:booky/viewcontroller/voucher_page.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_app/pages/HomePage.dart';
import 'package:flutter_app/pages/LandingPage.dart';

class RootPage extends StatefulWidget {

  RootPage({required this.auth, });

  final BaseAuth auth;

  @override
  State<StatefulWidget> createState() => new _RootPageState();
}

enum AuthStatus {
  NOT_DETERMINED,
  NOT_LOGGED_IN,
  LOGGED_IN,
}

//String _userId = "";

class _RootPageState extends State<RootPage> {
  AuthStatus authStatus = AuthStatus.NOT_DETERMINED;
  String _userId = "";
  //String userId = "";
  bool isLoggedIn = false;
  String name = '';
  late SharedPreferences prefs;
  String userphone = "";

  late Map<dynamic, dynamic> values;
  late String nama;
  late String email;
  late String password;
  late String keyuser = "";
  late String photoUrl, phonenumber, providerId, isEmailVerified, isAnonymous;
  late String telepon;

  @override
  void initState() {
    super.initState();
    widget.auth.getCurrentUser().then((user) {
      setState(() {
        if (user != null) {
          //userId = user?.uid;
          _userId = user.uid;

          //_userlog(user);

          var _db = FirebaseDatabase.instance.reference().child("users");
          _db.child(_userId);
          _db.once().then((DataSnapshot snapshot) {
            values = snapshot.value;
            //print(values.toString());
            values.forEach((k, v) {
              if (v["id"].toString() == _userId) {
                setState(() {
                keyuser = k.toString();
                nama = v["nama"].toString();
                email = v["email"].toString();
                password = v["password"].toString();
                telepon = v["telepon"].toString();
                phonenumber = v["telepon"].toString();
                });
              }
            });
            _userlog(user);
            authStatus = (user?.uid == null || phonenumber == null)? AuthStatus.NOT_LOGGED_IN : AuthStatus.LOGGED_IN;
          }
          );
        }
        else
          {
            authStatus = AuthStatus.NOT_LOGGED_IN;
          }
        //authStatus = user?.uid == null ? AuthStatus.NOT_LOGGED_IN : AuthStatus.LOGGED_IN;
      });
    });



    }

  //void _userlog( var user) {
  void _userlog( User user) {
    if (user != null) {
      //providerId = user.providerId.toString();
      name = user.displayName.toString();
      email = user.email.toString();
      photoUrl = user.photoURL.toString();
      isEmailVerified = user.emailVerified.toString();
      isAnonymous = user.isAnonymous.toString();
    }
  }


  void _onLoggedIn() {
    widget.auth.getCurrentUser().then((user){
      setState(() {
        //userId = user.uid.toString();
        _userId = user.uid.toString();
      });
    });
    setState(() {
      authStatus = AuthStatus.LOGGED_IN;

    });
  }

  void _onSignedOut() {
    setState(() {
      authStatus = AuthStatus.NOT_LOGGED_IN;
      _userId = "";
    });
  }

  Widget _buildWaitingScreen() {
    return Scaffold(
      body: Container(
        alignment: Alignment.center,
        child: CircularProgressIndicator(),
      ),
    );
  }



  @override
  Widget build(BuildContext context) {
    switch (authStatus) {
      case AuthStatus.NOT_DETERMINED:
        return _buildWaitingScreen();
        break;
      case AuthStatus.NOT_LOGGED_IN:
        return new LandingPage(
          auth: widget.auth,
          onSignedIn: _onLoggedIn,
          onSignedOut: _onSignedOut,
         );
        break;
      case AuthStatus.LOGGED_IN:
        if (_userId.length > 0 && _userId != null && phonenumber !=null) {
              isLoggedIn = true;
/*
              return new Home(
                userId: _userId,
                name: name,
                phone: phonenumber,
                email: email,
                password: password,
                isEmailVerified: isEmailVerified,
                isAnonymous: isAnonymous,
                auth: widget.auth,
                onSignedOut: _onSignedOut,
                indexpage: 0,
              );
*/
          return new HomePage(indexpage: 0, auth: Auth(), userId: "", name: "", email: "", password: "",
              phone: "", isEmailVerified: "", isAnonymous: "");
        } else return _buildWaitingScreen(); //return _buildWaitingScreen();
        break;
      default:
        return _buildWaitingScreen();
    }
  }


}
