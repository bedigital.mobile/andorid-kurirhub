import 'package:flutter/material.dart';
import 'package:flutter_app/animation/ScaleRoute.dart';
import 'package:flutter_app/pages/SignInPage.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter_app/widgets/SearchAddressWidget.dart';

import 'package:flutter_app/widgets/Dimension.dart';


class OrderDetail extends StatefulWidget {
  @override
  _OrderDetailState createState() => _OrderDetailState();
}

class _OrderDetailState extends State<OrderDetail> {
  bool valueasreceiver = false;
  int _n = 0;
  @override
  Widget build(BuildContext context) {
    String defaultFontFamily = 'Roboto-Light.ttf';

    return Scaffold(
      appBar: AppBar(
        title: Text('Rincian Pesanan'),
        centerTitle: true,
      ),
      body: Container(
        padding: EdgeInsets.only(left: 20, right: 20, top: 35, bottom: 30),
        width: double.infinity,
        height: double.infinity,
        color: Colors.white70,
        child: Column(
          children: [
            Card(
              clipBehavior: Clip.antiAlias,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              child: InkWell(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      flex: 2,
                      child: Column(
                        children: [
                          Expanded(
                            flex: 1,
                            child: IconTheme(
                                data: IconThemeData(
                                    color: Colors
                                        .amberAccent.shade700),
                                child: Icon(
                                  Icons.flight,
                                )),
                          ),
                          /*
                    Expanded(
                      flex: 1,
                      child: Text(
                          "${snapshot.data.carriers[index].name}",
                          style:
                          TextStyle(color: Colors.blue)),
                    ),

                     */
                        ],
                      ),
                    ),
                    Expanded(
                      flex: 4,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Expanded(
                            flex: 3,
                            child: Row(
                              mainAxisAlignment:
                              MainAxisAlignment.center,
                              children: [
                                /*
                          Consumer<SearchViewModel>(builder:
                              (context, item, child) {
                            return FutureBuilder<String>(
                              future: _getInboundCityName(),
                              builder: (BuildContext context,
                                  AsyncSnapshot snapshot) {
                                return Expanded(
                                  flex: 1,
                                  child: Text(
                                    "${snapshot.data}",
                                    style: TextStyle(
                                        fontSize: 20,
                                        fontWeight:
                                        FontWeight.w400),
                                  ),
                                );
                              },
                            );
                          }),

                           */
                                Expanded(
                                    flex: 1,
                                    child:
                                    Icon(Icons.trending_flat)),
                                /*
                          Consumer<SearchViewModel>(builder:
                              (context, item, child) {
                            return FutureBuilder<String>(
                              future: _getOutboundCityName(),
                              builder: (BuildContext context,
                                  AsyncSnapshot snapshot) {
                                return Expanded(
                                  flex: 2,
                                  child: Text(
                                    "${snapshot.data}",
                                    style: TextStyle(
                                        fontSize: 20,
                                        fontWeight:
                                        FontWeight.w400),
                                  ),
                                );
                              },
                            );
                          }),

                           */
                              ],
                            ),
                          ),
                          /*
                    snapshot.data.quotes[index].direct
                        ? Expanded(
                      flex: 1,
                      child: Text("Direct",
                          style:
                          TextStyle(fontSize: 14)),
                    )
                        : Expanded(
                      flex: 1,
                      child: Text("Not Direct",
                          style:
                          TextStyle(fontSize: 14)),
                    ),

                     */
                        ],
                      ),
                    ),
                    Expanded(
                      flex: 2,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          /*
                    Container(
                      child: Text(
                          "${snapshot.data.quotes[index].minPrice} TRY"),
                    ),


                    IconButton(
                      icon: Icon(
                        Icons.star,
                      ),

                      color: context
                          .read<AirportViewModel>()
                          .isPressed ==
                          true
                          ? Colors.yellow
                          : Colors.black,

                      onPressed: () {
                        context
                            .read<AirportViewModel>()
                            .onPressed();
                    ),

                     */
                        ],
                      ),
                    ),



                  ],
                ),
              ),
            ),

            Card(
              clipBehavior: Clip.antiAlias,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              child: InkWell(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      flex: 2,
                      child: Column(
                        children: [
                          Expanded(
                            flex: 1,
                            child: IconTheme(
                                data: IconThemeData(
                                    color: Colors
                                        .amberAccent.shade700),
                                child: Icon(
                                  Icons.flight,
                                )),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      flex: 4,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Expanded(
                            flex: 3,
                            child: Row(
                              mainAxisAlignment:
                              MainAxisAlignment.center,
                              children: [
                                Expanded(
                                    flex: 1,
                                    child:
                                    Icon(Icons.trending_flat)),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      flex: 2,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [

                        ],
                      ),
                    ),



                  ],
                ),
              ),
            ),
          ],
        )
      )
    );
  }

}

class SaveButtonWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: new BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(5.0)),
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: Color(0xFFfbab66),
          ),
          BoxShadow(
            color: Color(0xFFf7418c),
          ),
        ],
        gradient: new LinearGradient(
            colors: [Color(0xFFf7418c), Color(0xFFfbab66)],
            begin: const FractionalOffset(0.2, 0.2),
            end: const FractionalOffset(1.0, 1.0),
            stops: [0.0, 1.0],
            tileMode: TileMode.clamp),
      ),
      child: MaterialButton(
          highlightColor: Colors.transparent,
          splashColor: Color(0xFFf7418c),
          //shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(5.0))),
          child: Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 10.0, horizontal: 42.0),
            child: Text(
              "SIMPAN",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 25.0,
                  fontFamily: "WorkSansBold"),
            ),
          ),
          onPressed: () => {}),
    );
  }
}

