//import 'package:booky/viewcontroller/confirm_city_page.dart';
import 'package:flutter_app/pages/SignInPage.dart';
//import 'package:flutter_app/viewcontroller/register_page.dart';
import 'package:flutter/material.dart';
import 'package:page_view_indicator/page_view_indicator.dart';
import 'package:shared_preferences/shared_preferences.dart';
//import 'homeplace_page.dart';
import 'package:flutter_app/services/authentication.dart';
import 'package:flutter_app/pages/SignUpPage.dart';
import 'package:flutter_app/pages/HomePage.dart';


class LandingPage extends StatefulWidget {
  static const ROUTE = '/landing';
  LandingPage({required this.auth, required this.onSignedIn, required this.onSignedOut});

  final BaseAuth auth;
  final VoidCallback onSignedIn;
  final VoidCallback onSignedOut;

  @override
  _LandingPageState createState() => _LandingPageState();
}

enum AuthStatus {
  NOT_DETERMINED,
  NOT_LOGGED_IN,
  LOGGED_IN,
}


class _LandingPageState extends State<LandingPage> {

  bool isLoggedIn = false;
  String name = '';
  String _userId = "";
  AuthStatus authStatus = AuthStatus.NOT_DETERMINED;

  @override
  void initState() {
    super.initState();

  }

  void autoLogIn() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String userphone = prefs.getString('currentuserphone')!;
    if (userphone != null) {
      setState(() {
        isLoggedIn = true;
        Navigator.pushReplacement(context, new MaterialPageRoute(
            builder: (context) => new HomePage(indexpage: 0, auth: Auth(), userId: "", name: "", email: "", password: "",
                phone: "", isEmailVerified: "", isAnonymous: ""))
        );
      });
      return;
    }
  }

  Future<Null> logout() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('userphone', '');
    setState(() {
      name = '';
      isLoggedIn = false;
    });
  }

  final wallpapers = <String>[
    'assets/1.jpg',
  ];
  //static const length = 3;
  static const length = 1;
  final pageIndexNotifier = ValueNotifier<int>(0);

  @override
  Widget build(BuildContext context) {
    //return MaterialApp(


    return Scaffold(
      body: Container(
        decoration: new BoxDecoration(
          color: Colors.black,
          image: new DecorationImage(
              image: AssetImage(wallpapers[pageIndexNotifier.value]),
            fit: BoxFit.fill,
          ),
        ),
        child: SafeArea(
          child: new Column(
            //mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              _sliders,
              _buttons,
            ],
          ),
        ),
      ),
    );
    //);
  }

  Widget get _sliders => Expanded(
          child: Stack(
        //alignment: FractionalOffset.bottomCenter,
            alignment: FractionalOffset.topCenter,
        children: <Widget>[
          PageView.builder(
            onPageChanged: (index) =>
                setState(() => pageIndexNotifier.value = index),
            itemCount: length,
            itemBuilder: (context, index) {
              if (index == 0) {
                return _page1(context);
              } else if (index == 1) {
                return _page2(context);
              }
              return _page3(context);
            },
          ),
          _indicator
        ],
      ));

  WidgetBuilder get _page1 => (context) => Container(
        //padding: const EdgeInsets.symmetric(vertical: 32.0),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          ///crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
          ],
        ),
      );

  WidgetBuilder get _page2 => (context) => Container(
        //padding: const EdgeInsets.symmetric(vertical: 32.0),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          //crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
          ],
        ),
      );

  WidgetBuilder get _page3 => (context) => Container(
        //padding: const EdgeInsets.symmetric(vertical: 32.0),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          //crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
          ],
        ),
      );

  Widget get _indicator => PageViewIndicator(
        pageIndexNotifier: pageIndexNotifier,
        length: length,
        normalBuilder: (animationController, index) => Circle(
              size: 8.0,
              color: Colors.white30,
            ),
        highlightedBuilder: (animationController, index) => ScaleTransition(
              scale: CurvedAnimation(
                parent: animationController,
                curve: Curves.ease,
              ),
              child: Circle(
                size: 8.0,
                color: Colors.white,
              ),
            ),
      );

  Widget get _buttons => Container(
        width: MediaQuery.of(context).size.width,
        //padding: const EdgeInsets.symmetric(horizontal: 32.0),
          padding: const EdgeInsets.symmetric(horizontal: 32.0, vertical: 64.0),

        child: Column(
          //mainAxisSize: MainAxisSize.min,
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[

            Row(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[

                Expanded(
                  child: RaisedButton.icon(
                  onPressed: () {
                    Navigator.push(
                        context,
                        new MaterialPageRoute(
                            //builder: (context) => LoginPage(auth: new Auth()),
                            builder: (context) => SignInPage(),
                            fullscreenDialog: true));
                    //_signOut();
                  },

                  icon: Icon(Icons.person),
                  label: Text('Sign Up'),
                  color: Theme.of(context).primaryColor,
                    //color: Theme.of(context).primaryColorDark,
                  textColor: Colors.white,
                  ),
                ),


                SizedBox(width: 16.0),
                Expanded(
                  child: OutlineButton.icon(
                    onPressed: () {
                      Navigator.push(
                          context,
                          new MaterialPageRoute(
                              builder: (context) =>
                              /*
                                  LoginSignUpPage(
                                    auth: widget.auth,
                                    onSignedIn: _onLoggedIn,
                                  ), fullscreenDialog: true));
*/
                      SignUpPage(), fullscreenDialog: true));
                    },
                    icon: Icon(Icons.person),
                    label: Text('Sign In'),
                    textColor: Colors.white,
                    borderSide: BorderSide(color: Colors.white),
                  ),
                )
              ],
            )

          ],
        ),
      );

  _signOut() async {
    try {
      await widget.auth.signOut();
      widget.onSignedOut();
    } catch (e) {
      print(e);
    }
  }

  void _onLoggedIn() {
    widget.auth.getCurrentUser().then((user) {
      setState(() {
        _userId = user.uid.toString();
      });
    });

    setState(() {
      authStatus = AuthStatus.LOGGED_IN;
    });
  }
}
