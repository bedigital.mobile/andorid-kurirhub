import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/pages/FoodDetailsPage.dart';
import 'package:flutter_app/pages/FoodOrderPage.dart';
import 'package:flutter_app/pages/HomePage.dart';
import 'package:flutter_app/pages/SignInPage.dart';
import 'package:flutter_app/pages/SignUpPage.dart';
import 'package:flutter_app/services/authentication.dart';

import 'package:firebase_core/firebase_core.dart';
import 'package:splashscreen/splashscreen.dart';

/*
void main() => runApp(MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(fontFamily: 'Roboto', hintColor: Color(0xFFd0cece)),
      home: HomePage(indexpage: 0, auth: auth, userId: widget.userId, name: widget.name, email: widget.email, password: widget.password,
          phone: widget.phone, isEmailVerified: widget.isEmailVerified, isAnonymous: widget.isAnonymous, onSignedOut: widget.onSignedOut),
));
*/


void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}
class MyApp extends StatefulWidget {

  @override
  _MyAppState createState() => new _MyAppState();
}

class _MyAppState extends State<MyApp> {


  void initState() {
  }


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      //home: Splash(),
        home: new  HomePage(indexpage: 1, auth: Auth(), userId: "", name: "", email: "", password: "",
            phone: "", isEmailVerified: "", isAnonymous: "")
    );
  }
}

class Splash extends StatelessWidget{
  @override
  Widget build(BuildContext context) {

    return new SplashScreen(
        seconds: 1,
        navigateAfterSeconds: new MyApp1(),
        title: new Text('Loading...',
          style: new TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 20.0
          ),),
        image: new Image.asset('assets/booky_logo.png'),
        backgroundColor: Colors.white,
        styleTextUnderTheLoader: new TextStyle(),
        photoSize: 100.0,
        loaderColor: Colors.red
    );
  }
}

class MyApp1 extends StatelessWidget {

  @override

  Widget build(BuildContext context) {
    return MaterialApp(

      title: 'Booky',
      debugShowCheckedModeBanner: false,

      theme: ThemeData(
        brightness: Brightness.light,
        primarySwatch: Colors.teal,
        primaryColor: Colors.teal,
      ),

      darkTheme: ThemeData(
        brightness: Brightness.dark,
      ),

      //-------sementara----------
     // home: new RootPage(auth: new Auth(),),
        home: new  HomePage(indexpage: 1, auth: Auth(), userId: "", name: "", email: "", password: "",
            phone: "", isEmailVerified: "", isAnonymous: "")
      //--------------------------

    );
  }
}


