import 'package:flutter/material.dart';

class SearchAddressWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 0,top: 5,right: 0,bottom: 5),
      child: TextField(
        decoration: InputDecoration(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(5.0)),
            borderSide: BorderSide(
              width: 0,
              color: Color(0xFFfb3132),
              style: BorderStyle.none,
            ),
          ),
          filled: true,
          suffixIcon: Icon(
            Icons.search,
            color: Color(0xFFfb3132),
          ),
          fillColor: Color(0xFFFAFAFA),
          //suffixIcon: Icon(Icons.sort,color: Color(0xFFfb3132),),
          hintStyle: new TextStyle(color: Color(0xFFd0cece), fontSize: 18),
          hintText: "Pilih Alamat",
        ),
      ),

    );
  }
}
