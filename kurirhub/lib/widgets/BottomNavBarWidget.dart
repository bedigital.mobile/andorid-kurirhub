import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:flutter_app/pages/HomePage.dart';
import 'package:flutter_app/pages/InboxPage.dart';
import 'package:flutter_app/pages/TransHistPage.dart';
import 'package:flutter_app/pages/AccountPage.dart';
import 'package:flutter_app/services/authentication.dart';

class BottomNavBarWidget extends StatefulWidget {
  BottomNavBarWidget({required this.auth,required this.userId, required this.name,required this.email,required this.password,
    required this.phone,required this.isEmailVerified,required this.isAnonymous, required  this.indexpage
  });

  final BaseAuth auth;
  final String userId;
  final String name;
  final String email;
  final String password;
  final String phone;
  final String isEmailVerified;
  final String isAnonymous;
  final int indexpage;
  @override
  _BottomNavBarWidgetState createState() => _BottomNavBarWidgetState();
}



class _BottomNavBarWidgetState extends State<BottomNavBarWidget> {

  int currentTab = 0;
  late List<Widget> pages;
  late Widget currentPage;
  late HomePage homepage;
  late InboxPage inboxpage;
  late TransHistPage transhistpage;
  late AccountPage accountpage;

  @override
  void initState() {
    super.initState();

    homepage=new HomePage(
        indexpage: 0, auth: widget.auth, userId: widget.userId, name: widget.name, email: widget.email, password: widget.password,
        phone: widget.phone, isEmailVerified: widget.isEmailVerified, isAnonymous: widget.isAnonymous
    );

    inboxpage=new InboxPage(auth: widget.auth, userId: widget.userId, name: widget.name, email: widget.email,
        password: widget.password, phone: widget.phone, isEmailVerified: widget.isEmailVerified, isAnonymous: widget.isAnonymous);

    transhistpage=new TransHistPage(auth: widget.auth, userId: widget.userId, name: widget.name, email: widget.email, password: widget.password,
        phone: widget.phone, isEmailVerified: widget.isEmailVerified, isAnonymous: widget.isAnonymous);

    accountpage=new AccountPage(auth: widget.auth, userId: widget.userId, name: widget.name, email: widget.email, password: widget.password,
        phone: widget.phone, isEmailVerified: widget.isEmailVerified, isAnonymous: widget.isAnonymous);

    pages=[ homepage, inboxpage, transhistpage, accountpage];
    currentPage =  pages[widget.indexpage];
  }


  @override
  Widget build(BuildContext context) {
    int _selectedIndex = 0;
    void _onItemTapped(int index) {
      setState(() {
        _selectedIndex = index;
        //navigateToScreens(index);
        currentTab=_selectedIndex;
        currentPage=pages[_selectedIndex];
      });
    }

    return BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      items: const <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(Icons.home),
          title: Text(
            'Beranda',
            style: TextStyle(color: Color(0xFF2c2b2b)),
          ),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.mail),
          title: Text(
            'Kotak Masuk',
            style: TextStyle(color: Color(0xFF2c2b2b)),
          ),

        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.card_giftcard),
          title: Text(
            'Transaksi Saya',
            style: TextStyle(color: Color(0xFF2c2b2b)),
          ),
        ),
        BottomNavigationBarItem(
          icon: Icon(FontAwesomeIcons.user),
          title: Text(
            'Akun',
            style: TextStyle(color: Color(0xFF2c2b2b)),
          ),
        ),
      ],
      currentIndex: _selectedIndex,
      selectedItemColor: Color(0xFFfd5352),
      onTap: _onItemTapped,
    );
  }
}
