import 'package:flutter/material.dart';

import 'package:flutter_app/animation/ScaleRoute.dart';
import 'package:flutter_app/pages/NewPackageDeliveryTrackingPage.dart';

class TopMenus extends StatefulWidget {
  @override
  _TopMenusState createState() => _TopMenusState();
}

class _TopMenusState extends State<TopMenus> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: <Widget>[
          TopMenuTiles(name: "Kirim", imageUrl: "kirim", slug: ""),
          TopMenuTiles(name: "Cek Tarif", imageUrl: "cektarif", slug: ""),
          TopMenuTiles(name: "Cek Resi", imageUrl: "cekresi", slug: ""),
          /*
          TopMenuTiles(name: "Cake", imageUrl: "ic_cake", slug: ""),
          TopMenuTiles(name: "Ice Cream", imageUrl: "ic_ice_cream", slug: ""),
          TopMenuTiles(name: "Soft Drink", imageUrl: "ic_soft_drink", slug: ""),
          TopMenuTiles(name: "Burger", imageUrl: "ic_burger", slug: ""),
          TopMenuTiles(name: "Sushi", imageUrl: "ic_sushi", slug: ""),
           */
        ],
      ),
    );
  }
}

class TopMenuTiles extends StatelessWidget {
  String name;
  String imageUrl;
  String slug;

  TopMenuTiles(
      {Key? key,
      required this.name,
      required this.imageUrl,
        required this.slug})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      //onTap: () {},
      onTap: () {
        Navigator.push(context, ScaleRoute(page: NewPackageDeliveryTrackingPage()));
      },
      child: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(left: 10, right: 5, top: 5, bottom: 5),
            /*
            decoration: new BoxDecoration(boxShadow: [
              new BoxShadow(
                color: Color(0xFFfae3e2),
                blurRadius: 25.0,
                offset: Offset(0.0, 0.75),
              ),
            ]),
             */
            child: Card(
                color: Colors.white,
                elevation: 0,
                shape: RoundedRectangleBorder(
                  borderRadius: const BorderRadius.all(
                    Radius.circular(3.0),
                  ),
                ),

                child: Container(
                  width: 50,
                  height: 50,
                  child: Center(
                      child: Image.asset(
                    //'assets/images/topmenu/' + imageUrl + ".png",
                        'assets/images/' + imageUrl + ".png",
                    width: 24,
                    height: 24,
                  )
                  ),
                    decoration: BoxDecoration(
                      // The child of a round Card should be in round shape
                        shape: BoxShape.circle,
                        color: Colors.red[100]
                    )
                )
            ),
          ),
          Text(name,
              style: TextStyle(
                  color: Color(0xFF6e6e71),
                  fontSize: 14,
                  fontWeight: FontWeight.w400)),
        ],
      ),
    );
  }
}
