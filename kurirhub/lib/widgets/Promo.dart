import 'package:flutter/material.dart';

class Promo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        /*
        ListTile(
          title: Text('Promo',
              style: TextStyle(fontWeight:FontWeight.w600, fontSize: 22.0)),
          trailing: IconButton(
            icon: Icon(Icons.keyboard_arrow_right),
            onPressed: (){},
          ),
        ),
        */
        Container(
          padding: EdgeInsets.only(left: 10, right: 10, top: 5, bottom: 5),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                "Promo",
                style: TextStyle(
                    fontSize: 20,
                    color: Color(0xFF3a3a3b),
                    //fontWeight: FontWeight.w300),
                    fontWeight: FontWeight.bold),

              ),
              Text(
                "Lihat Semua",
                style: TextStyle(
                    fontSize: 16, color: Colors.blue, fontWeight: FontWeight.w100),
              )
            ],
          ),
        ),
        Container(
          width: double.infinity,
          height: 150.0,
          padding: EdgeInsets.only(left: 8.0),
          child: ListView(
            scrollDirection: Axis.horizontal,
            children: <Widget>[
              Container(
                height: 150.0,
                width: 150.0,
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                          Colors.blue,
                          Colors.blueAccent
                        ]
                    ),
                    borderRadius: BorderRadius.circular(8.0)
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      decoration: BoxDecoration(
                          color: Colors.red[300],
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.elliptical(20.0, 20.0),
                              bottomRight: Radius.elliptical(150.0, 150.0)
                          )
                      ),
                      child: Padding(
                        padding: const EdgeInsets.only(top: 2.0, left: 5.0, right: 30.0, bottom: 30.0),
                        child: Text('%', style:TextStyle(fontSize: 24.0, color: Colors.white)),
                      ),
                    ),
                    Expanded(child: Container(),),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text('Lihat Semua \nPromo',
                        style: TextStyle(fontWeight: FontWeight.w500,
                            color: Colors.white,
                            fontSize: 18.0),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                height: 150.0,
                width: 300.0,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8.0),
                  image: DecorationImage(
                      image: AssetImage('assets/images/promo/promo1.jpeg')
                  ),
                ),
                margin: EdgeInsets.only(left: 10.0),
                child: null,
              ),
              Container(
                height: 150.0,
                width: 300.0,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8.0),
                  image: DecorationImage(
                      image: AssetImage('assets/images/promo/promo2.jpeg')
                  ),
                ),
                margin: EdgeInsets.only(left: 10.0),
                child: null,
              )
            ],
          ),
        )
      ],
    );
  }
}