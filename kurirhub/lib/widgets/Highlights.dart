import 'package:flutter/material.dart';
import 'package:flutter_app/animation/RotationRoute.dart';
import 'package:flutter_app/animation/ScaleRoute.dart';
import 'package:flutter_app/pages/FoodDetailsPage.dart';

class Highlight extends StatefulWidget {
  @override
  _HighlightsWidgetState createState() => _HighlightsWidgetState();
}

class _HighlightsWidgetState extends State<Highlight> {
  @override
  Widget build(BuildContext context) {
    return Container(
    //return Flexible(
      //height: 265,
      height: 320,
      width: double.infinity,
      child: Column(
        children: <Widget>[
          HighlightTitle(),
          Expanded(
            child: HighlightItems(),
          )
        ],
      ),
    );
  }
}

class HighlightTiles extends StatelessWidget {
  String name;
  String imageUrl;
  String rating;
  String numberOfRating;
  String price;
  String slug;
  String description;

  HighlightTiles(
      {Key? key,
      required this.name,
      required this.imageUrl,
      required this.rating,
      required this.numberOfRating,
      required this.price,
      required this.slug,
      required this.description
})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(context, ScaleRoute(page: FoodDetailsPage()));
      },
      child: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(left: 10, right: 5, top: 5, bottom: 5),
            decoration: BoxDecoration(boxShadow: [
              /* BoxShadow(
                color: Color(0xFFfae3e2),
                blurRadius: 15.0,
                offset: Offset(0, 0.75),
              ),*/
            ]),
            child: Card(
                color: Colors.white,
                elevation: 0,
                shape: RoundedRectangleBorder(
                  borderRadius: const BorderRadius.all(
                    Radius.circular(5.0),
                  ),
                ),
                child: Container(
                  width: 170,
                  //height: 210,
                  //height: 500,
                  child: Column(
                    children: <Widget>[
                      Stack(
                        children: <Widget>[
                          Align(
                            alignment: Alignment.topRight,
                            child: Container(
                              alignment: Alignment.topRight,
                              width: double.infinity,
                              padding: EdgeInsets.only(right: 5, top: 5),
                              child: Container(
                                height: 28,
                                width: 28,
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Colors.white70,
                                    boxShadow: [
                                      BoxShadow(
                                        color: Color(0xFFfae3e2),
                                        blurRadius: 25.0,
                                        offset: Offset(0.0, 0.75),
                                      ),
                                    ]),
                                child: Icon(
                                  Icons.favorite,
                                  color: Color(0xFFfb3132),
                                  size: 16,
                                ),
                              ),
                            ),
                          ),
                          Align(
                            alignment: Alignment.centerLeft,
                            child: Center(
                                child: Image.asset(
                              'assets/images/highlights/' +
                                  imageUrl +
                                  ".jpg",
                              width: 130,
                              height: 140,
                            )),
                          )
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                        /*
                          Container(
                            alignment: Alignment.bottomLeft,
                            padding: EdgeInsets.only(left: 5, top: 5),

                            child: Text(name,
                                style: TextStyle(
                                    color: Color(0xFF6e6e71),
                                    fontSize: 15,
                                    fontWeight: FontWeight.w500)
                            ),
                          ),
                         */
                          Flexible(
                              child: Container(
                                /*
                                child: Text(name,
                                    style: TextStyle(
                                        color: Color(0xFF6e6e71),
                                        fontSize: 15,
                                        //fontWeight: FontWeight.w500)
                                        fontWeight: FontWeight.bold)
                                ),
                                 */
                                child: Column(
                                  children: [
                                    Text(name,
                                        style: TextStyle(
                                            color: Color(0xFF6e6e71),
                                            fontSize: 15,
                                            //fontWeight: FontWeight.w500)
                                            fontWeight: FontWeight.bold)
                                    ),

                                    Text(description,
                                        style: TextStyle(
                                            color: Color(0xFF6e6e71),
                                            fontSize: 15,
                                            fontWeight: FontWeight.w500),
                                          maxLines: 4,
                                          overflow: TextOverflow.fade,
                                          //softWrap: false,
                                    ),

                                    Text("Lihat Lebih",
                                      style: TextStyle(
                                          color: Colors.redAccent,
                                          fontSize: 15,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ],
                                )
                              )
                          ),
                          /*
                          Flexible(
                              child: Container(
                                child: Text(description,
                                    style: TextStyle(
                                        color: Color(0xFF6e6e71),
                                        fontSize: 15,
                                        fontWeight: FontWeight.w500)
                                ),
                              )
                          ),


                          Container(
                            alignment: Alignment.topRight,
                            padding: EdgeInsets.only(right: 5),
                            child: Container(
                              height: 28,
                              width: 28,
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: Colors.white70,
                                  boxShadow: [
                                    BoxShadow(
                                      color: Color(0xFFfae3e2),
                                      blurRadius: 25.0,
                                      offset: Offset(0.0, 0.75),
                                    ),
                                  ]),
                              child: Icon(
                                Icons.near_me,
                                color: Color(0xFFfb3132),
                                size: 16,
                              ),
                            ),
                          ),

                           */
                        ],
                      ),
/*
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Container(
                                alignment: Alignment.topLeft,
                                padding: EdgeInsets.only(left: 5, top: 5),
                                child: Text(rating,
                                    style: TextStyle(
                                        color: Color(0xFF6e6e71),
                                        fontSize: 10,
                                        fontWeight: FontWeight.w400)),
                              ),
                              Container(
                                padding: EdgeInsets.only(top: 3, left: 5),
                                child: Row(
                                  children: <Widget>[
                                    Icon(
                                      Icons.star,
                                      size: 10,
                                      color: Color(0xFFfb3132),
                                    ),
                                    Icon(
                                      Icons.star,
                                      size: 10,
                                      color: Color(0xFFfb3132),
                                    ),
                                    Icon(
                                      Icons.star,
                                      size: 10,
                                      color: Color(0xFFfb3132),
                                    ),
                                    Icon(
                                      Icons.star,
                                      size: 10,
                                      color: Color(0xFFfb3132),
                                    ),
                                    Icon(
                                      Icons.star,
                                      size: 10,
                                      color: Color(0xFF9b9b9c),
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                alignment: Alignment.topLeft,
                                padding: EdgeInsets.only(left: 5, top: 5),
                                child: Text("($numberOfRating)",
                                    style: TextStyle(
                                        color: Color(0xFF6e6e71),
                                        fontSize: 10,
                                        fontWeight: FontWeight.w400)),
                              ),
                            ],
                          ),
                          Container(
                            alignment: Alignment.bottomLeft,
                            padding: EdgeInsets.only(left: 5, top: 5, right: 5),
                            child: Text('\$' + price,
                                style: TextStyle(
                                    color: Color(0xFF6e6e71),
                                    fontSize: 12,
                                    fontWeight: FontWeight.w600)),
                          )
                        ],
                      )

 */
                    ],
                  ),
                )),
          ),
        ],
      ),
    );
  }
}

class HighlightTitle extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 10, right: 10, top: 5, bottom: 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            "Highlight",
            style: TextStyle(
                fontSize: 20,
                color: Color(0xFF3a3a3b),
                //fontWeight: FontWeight.w300),
                fontWeight: FontWeight.bold),

          ),
          Text(
            "Lihat Semua",
            style: TextStyle(
                fontSize: 16, color: Colors.blue, fontWeight: FontWeight.w100),
          )
        ],
      ),
    );
  }
}

class HighlightItems extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      scrollDirection: Axis.horizontal,
      children: <Widget>[
        HighlightTiles(
            name: "Kirim Paket Sameday Gojek",
            imageUrl: "1",
            rating: '4.9',
            numberOfRating: '200',
            price: '15.06',
            slug: "fried_egg",
            description: "Di bulan kasih sayang ini kirim-kirim pake dibikin BAPER yang bahagia! Dison 35 %",),
        HighlightTiles(
            name: "Mixed Vegetable",
            imageUrl: "2",
            rating: "4.9",
            numberOfRating: "100",
            price: "17.03",
            slug: "",
          description: "Di bula kasih sayang ini kirim-kirim pake dibikin BAPER yang bahagia! Dison 35 %",),
        HighlightTiles(
            name: "Salad With Chicken",
            imageUrl: "3",
            rating: "4.0",
            numberOfRating: "50",
            price: "11.00",
            slug: "",
          description: "Di bula kasih sayang ini kirim-kirim pake dibikin BAPER yang bahagia! Dison 35 %",),
        HighlightTiles(
            name: "Mixed Salad",
            imageUrl: "4",
            rating: "4.00",
            numberOfRating: "100",
            price: "11.10",
            slug: "",
          description: "Di bula kasih sayang ini kirim-kirim pake dibikin BAPER yang bahagia! Dison 35 %",),
        HighlightTiles(
            name: "Red meat,Salad",
            imageUrl: "5",
            rating: "4.6",
            numberOfRating: "150",
            price: "12.00",
            slug: "",
          description: "Di bula kasih sayang ini kirim-kirim pake dibikin BAPER yang bahagia! Dison 35 %",),
        /*
        HighlightTiles(
            name: "Mixed Salad",
            imageUrl: "ic_popular_food_5",
            rating: "4.00",
            numberOfRating: "100",
            price: "11.10",
            slug: ""),
        HighlightTiles(
            name: "Potato,Meat fry",
            imageUrl: "ic_popular_food_6",
            rating: "4.2",
            numberOfRating: "70",
            price: "23.0",
            slug: ""),
        HighlightTiles(
            name: "Fried Egg",
            imageUrl: "ic_popular_food_1",
            rating: '4.9',
            numberOfRating: '200',
            price: '15.06',
            slug: "fried_egg"),
        HighlightTiles(
            name: "Red meat,Salad",
            imageUrl: "ic_popular_food_2",
            rating: "4.6",
            numberOfRating: "150",
            price: "12.00",
            slug: ""),

         */
      ],
    );
  }
}


